//
//  TaskTableViewCell.swift
//  Prima 30 agosto
//
//  Created by Lucio Botteri on 01/09/21.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taskImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        taskImageView.layer.cornerRadius = taskImageView.frame.height/2
    }
    
    func configure(with taskIndex: Int) {
        let task = AppData.shared.tasks[taskIndex]
        titleLabel.text = task.name
        if let data = task.imageData {
            taskImageView.image = UIImage(data: data)
        } else {
            taskImageView.image = UIImage(systemName: "questionmark.circle")
        }
    }
}
