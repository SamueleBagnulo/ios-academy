//
//  ViewController.swift
//  Prima 30 agosto
//
//  Created by Lucio Botteri on 30/08/21.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet private weak var taskTableView: UITableView!
    
    private var taskToEditIndex: Int?
    
    private var completedTasks: [Task] {
        AppData.shared.tasks.filter { $0.completed }
    }
    
    private var uncompletedTasks: [Task] {
        AppData.shared.tasks.filter { !$0.completed }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destVC = segue.destination as? AddNewTaskVC {
            destVC.configure(with: taskToEditIndex, delegate: self)
        }
    }
    
    @IBAction private func addButtonDidPress(_ sender: UIBarButtonItem) {
        taskToEditIndex = nil
        performSegue(withIdentifier: "toAddTaskVC", sender: self)
    }
}

// MARK: - Table View

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int { 2 }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return completedTasks.count
        } else {
            return uncompletedTasks.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell", for: indexPath) as! TaskTableViewCell
        let task = indexPath.section == 0 ? completedTasks[indexPath.row] : uncompletedTasks[indexPath.row]
        if let i = AppData.shared.tasks.firstIndex(of: task) {
            cell.configure(with: i)
        }
        cell.accessoryType = task.completed ? .checkmark : .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tasks = indexPath.section == 0 ? completedTasks : uncompletedTasks
        tasks[indexPath.row].completed.toggle()
        AppData.shared.tasks.sort()
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let taskToRemove: Task
        if indexPath.section == 0 {
            taskToRemove = completedTasks[indexPath.row]
        } else {
            taskToRemove = uncompletedTasks[indexPath.row]
        }
        if editingStyle == .delete {
            AppData.shared.tasks.removeAll { $0 == taskToRemove }
            AppData.shared.saveTasks()
            taskTableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let actionProvider: UIContextMenuActionProvider = { _ in
            let menu = UIMenu(children: [
                UIAction(title: "Modifica", image: UIImage(systemName: "pencil")!) { [self] _ in
                    let taskToEdit = indexPath.section == 0 ? completedTasks[indexPath.row] : uncompletedTasks[indexPath.row]
                    taskToEditIndex = AppData.shared.tasks.firstIndex(of: taskToEdit)
                    performSegue(withIdentifier: "toAddTaskVC", sender: self)
                }
            ])
            return menu
        }
        return UIContextMenuConfiguration(identifier: "Boh" as NSCopying, previewProvider: nil, actionProvider: actionProvider)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Completati"
        default:
            return "Da completare"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
}

// MARK: - Add New Task Delegate Implementation

extension HomeViewController: AddNewTaskVCDelegate {
    
    func taskDidAdd(_ task: Task) {
        AppData.shared.tasks.append(task)
        AppData.shared.saveTasks()
        let indexPath = IndexPath(row: uncompletedTasks.count-1, section: 1)
        taskTableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    func taskDidEdit(at taskIndex: Int, newName: String, newImageData: Data?) {
        let task = AppData.shared.tasks[taskIndex]
        task.name = newName
        task.imageData = newImageData
        AppData.shared.saveTasks()
        taskTableView.reloadData()
    }
}
