//
//  AddNewTaskVC.swift
//  Prima 30 agosto
//
//  Created by Lucio Botteri on 30/08/21.
//

import UIKit

protocol AddNewTaskVCDelegate: AnyObject {
    func taskDidAdd(_ task: Task)
    func taskDidEdit(at taskIndex: Int, newName: String, newImageData: Data?)
}

class AddNewTaskVC: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet private weak var taskTextField: UITextField!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var imageButton: UIButton!
    @IBOutlet private weak var taskImageView: UIImageView!
    
    private weak var delegate: AddNewTaskVCDelegate?
    private var taskIndex: Int?
    private lazy var imagePicker: UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.delegate = self
        return picker
    }()
    private var newImageData: Data? {
        taskImageView.image?.pngData()
    }
    
    // MARK: - View Controller Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let i = taskIndex {
            descriptionLabel.text = "Modifica nome task:"
            taskTextField.text = AppData.shared.tasks[i].name
            if let data = AppData.shared.tasks[i].imageData {
                taskImageView.image = UIImage(data: data)
            }
        } else {
            descriptionLabel.text = "Inserisci nome task:"
        }
        refreshTextField()
    }
    
    // MARK: - Interface Builder Actions
    
    @IBAction func saveDidPress(_ sender: UIButton) {
        if let safeText = taskTextField.text {
            if let i = taskIndex {
                delegate?.taskDidEdit(at: i, newName: safeText, newImageData: newImageData)
            } else {
                delegate?.taskDidAdd(Task(name: safeText, imageData: newImageData))
            }
        }
        dismiss(animated: true)
    }
    
    @IBAction func textDidChange(_ sender: UITextField) {
        refreshTextField()
    }
    
    @IBAction func imageButtonDidPress(_ sender: UIButton) {
        let alert = UIAlertController(title: "Carica una foto", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Scatta da fotocamera", style: .default) { [self] _ in
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true)
        }
        let libraryAction = UIAlertAction(title: "Importa da libreria", style: .default) { [self] _ in
            imagePicker.sourceType = .photoLibrary
            present(imagePicker, animated: true)
        }
        if taskImageView.image != nil {
            let deleteImageAction = UIAlertAction(title: "Elimina foto", style: .destructive) { [self] _ in
                taskImageView.image = nil
            }
            alert.addAction(deleteImageAction)
        }
        let cancelAction = UIAlertAction(title: "Annulla", style: .cancel)
        alert.addAction(cameraAction)
        alert.addAction(libraryAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    // MARK: - Private methods
    
    private func refreshTextField() {
        saveButton.isEnabled = !taskTextField.text!.isEmpty
    }
    
    // MARK: - Public methods
    
    func configure(with taskIndex: Int? = nil, delegate: AddNewTaskVCDelegate?) {
        self.taskIndex = taskIndex
        self.delegate = delegate
    }
}

// MARK: - Image Picker Delegate

extension AddNewTaskVC: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true)
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        taskImageView.image = selectedImage
    }
}
