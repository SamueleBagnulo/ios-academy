//
//  Task.swift
//  Prima 30 agosto
//
//  Created by Lucio Botteri on 30/08/21.
//

import Foundation

class Task: Codable {
    var id: UUID
    var name: String
    var date: Date
    var completed: Bool
    var imageData: Data?
    
    init(name: String, imageData: Data? = nil) {
        self.name = name
        self.imageData = imageData
        completed = false
        date = Date()
        id = UUID()
    }
}

extension Task: Comparable {
    static func < (lhs: Task, rhs: Task) -> Bool {
        return (lhs.completed && !rhs.completed) || lhs.date < rhs.date
    }
    
    static func == (lhs: Task, rhs: Task) -> Bool {
        lhs.id == rhs.id
    }
}
