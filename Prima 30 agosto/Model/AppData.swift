//
//  AppData.swift
//  Prima 30 agosto
//
//  Created by Lucio Botteri on 31/08/21.
//

import Foundation

extension UserDefaults {
    func decode<T: Decodable>(_ type: T.Type, forKey defaultName: String) throws -> T {
        try JSONDecoder().decode(T.self, from: data(forKey: defaultName) ?? .init())
    }
    func encode<T: Encodable>(_ value: T, forKey defaultName: String) throws {
        try set(JSONEncoder().encode(value), forKey: defaultName)
    }
}

/// Singleton
final class AppData {
    
    static let shared = AppData()
    
    var tasks = [Task]()
    
    private init() {
        tasks = (try? UserDefaults.standard.decode([Task].self, forKey: "savedTasks")) ?? []
    }
    
    func saveTasks() {
        do {
            try UserDefaults.standard.encode(tasks, forKey: "savedTasks")
        } catch {
            print("Errore: \(error.localizedDescription)")
        }
    }
}
